const express = require('express');
const socket = require('socket.io');
const config = require('dotenv').config();
const fs = require('fs');

// setup the apps
const PORT = 4000;
const app = express();
const server = app.listen(PORT, () => {
  console.log(`Server is listening on port ${PORT}`);
  console.log(`http://localhost:${PORT}`);
});

// Static files
app.use(express.static('public'));

// Socket setup
const io = socket(server, {
  cors: {
    origin: `http://localhost:${PORT}`,
    methods: ['GET', 'POST'],
    credentials: true,
    transports: ['websocket', 'polling'],
  },
  allowEIO3: true,
});
const activeUsers = [];
const rooms = [];
let stateGame = 0; // 0 : Not Start / Waiting , 1: Played

function generateQuestion(qty, exclude) {
  const rawdata = fs.readFileSync('listquestion.json');
  let questions = JSON.parse(rawdata);

  // eslint-disable-next-line no-param-reassign
  exclude = Array.isArray(exclude) ? exclude : [];
  questions = questions.filter((r) => exclude.indexOf(r.id) === -1);
  questions = questions.sort(() => Math.random() - 0.5);

  const questionList = [];
  // eslint-disable-next-line no-plusplus
  for (let i = 1; i <= qty; i++) {
    const gradientRd = Math.floor(Math.random() * 5) + 1;
    questionList.push({ ...questions[i], gradientBg: gradientRd });
  }

  return questionList;
}

function changePlayerTurn(socket) {
  const env = config.parsed;
  const user = activeUsers.find((r) => r.socketId === socket.id);
  const room = rooms.find((r) => r.roomCode == user.roomCode);
  const { questionLeft, usedQuestionId } = room;
  const userPick = activeUsers[room.currentIdxPlayer];
  // eslint-disable-next-line max-len

  if (questionLeft == 0) {
    io.to(room.roomCode).emit('end_game');
  } else {
    // generate question
    const listQuestion = generateQuestion(env.QUESTION_QTY_PER_MATCH, usedQuestionId);
    io.to(userPick.socketId).emit('prepare_pick', listQuestion);

    let currentIdxPlayer = room.currentIdxPlayer + 1;
    currentIdxPlayer = currentIdxPlayer > (env.REQUIRED_PLAYER - 1) ? 0 : currentIdxPlayer;
    room.currentIdxPlayer = currentIdxPlayer;
  }
}

// eslint-disable-next-line no-shadow
io.on('connection', (socket) => {
  console.log('Socket.io connection ready!');
  socket.on('new_player', (username) => {
    console.log(socket.id, username);
    // eslint-disable-next-line no-param-reassign
    username = username.trim().toLowerCase();
    // check username first
    if (activeUsers.find((r) => r.username == username) != null) {
      socket.emit('username_exist', {
        username,
      });
    } else {
      const env = config.parsed;
      // eslint-disable-next-line no-param-reassign
      socket.userId = username;
      // find available or create new room
      let roomCode = '';
      let currentIdxPlayer = 0;
      const joinRoom = rooms.find((r) => r.users < env.REQUIRED_PLAYER);
      if (rooms.length == 0 || joinRoom == null) {
        roomCode = `rooms-${Math.floor(Math.random() * 1000000)}`;
        rooms.push({
          roomCode,
          users: 1,
          activeUsers: 1,
          questionLeft: env.QUESTION_QTY_PER_GAME,
          usedQuestionId: [],
          currentIdxPlayer: 0,
        });
      } else {
        roomCode = joinRoom.roomCode;
        currentIdxPlayer = joinRoom.roomCode;
        joinRoom.users += 1;
        joinRoom.activeUsers += 1;
      }
      console.log(joinRoom);
      socket.join(roomCode);

      activeUsers.push({ username, socketId: socket.id, roomCode });
      const msg = activeUsers.length < env.REQUIRED_PLAYER ? `WAITING FOR OTHER PLAYER ${activeUsers.length}/${env.REQUIRED_PLAYER}` : '';
      stateGame = activeUsers.length >= env.REQUIRED_PLAYER ? 1 : 0;
      io.to(roomCode).emit('new_player', {
        users: activeUsers, stateGame, currentIdxPlayer, msg, roomCode,
      });

      if (stateGame == 1) {
        changePlayerTurn(socket);
      }
    }
  });

  socket.on('draw_card', () => {
    const env = config.parsed;
    const user = activeUsers.find((r) => r.socketId === socket.id);
    const room = rooms.find((r) => r.roomCode == user.roomCode);
    const { questionLeft, usedQuestionId } = room;
    // eslint-disable-next-line max-len
    const qty = questionLeft < env.QUESTION_QTY_PER_MATCH ? questionLeft : env.QUESTION_QTY_PER_MATCH;
    const listQuestion = generateQuestion(qty, usedQuestionId);
    socket.emit('draw_card', listQuestion);
  });

  // this event trigerred when player pick question
  socket.on('pick_question', (data) => {
    const user = activeUsers.find((r) => r.socketId === socket.id);
    const room = rooms.find((r) => r.roomCode == user.roomCode);
    room.usedQuestionId.push(data.id);
    room.questionLeft -= 1;
    io.to(room.roomCode).emit('pick_question', [data]);
    socket.emit('show_turn');
  });

  socket.on('next_turn', () => {
    changePlayerTurn(socket);
  });

  socket.on('player_turn_on_prepare', () => {
    const user = activeUsers.find((r) => r.socketId === socket.id);
    socket.broadcast.emit('player_turn_on_prepare', { playerOnPrepare: user });
  });

  socket.on('disconnect', () => {
    const user = activeUsers.find((r) => r.socketId === socket.id);
    if (user != null) {
      const room = rooms.find((r) => r.roomCode == user.roomCode);
      room.activeUsers -= 1;

      // delete unused room
      if (room.activeUsers <= 0) {
        rooms.splice(rooms.findIndex((r) => r.roomCode === user.roomCode), 1);
        console.log('deleted Room');
      }

      // delete from active users
      activeUsers.splice(activeUsers.findIndex((r) => r.socketId === socket.id), 1);
    }
    // room will not decrease user , if user leave he cannot rejoin the room
    io.emit('user disconnected', socket.userId);
  });
});
